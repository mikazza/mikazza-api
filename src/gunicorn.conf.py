# Gunicorn configuration file
# https://docs.gunicorn.org/en/stable/configure.html#configuration-file
# https://docs.gunicorn.org/en/stable/settings.html

import multiprocessing
import os

PORT = os.environ.get("PORT") or 8000

bind = f"0.0.0.0:{PORT}"
workers = os.environ.get("GUNICORN_WORKER") or 1
log_file = "-"