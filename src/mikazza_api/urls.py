"""mikazza_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path, re_path
from rest_framework import routers, permissions
from mikazza_api.mikazza import views
from mikazza_api.mikazza.views import PrivateGraphQLView
from django.views.generic import TemplateView
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.views.generic import RedirectView
from graphene_django.views import GraphQLView
from mikazza_api.mikazza.schema.schema import schema
from graphene_file_upload.django import FileUploadGraphQLView
from rest_framework_simplejwt.views import (TokenObtainPairView,TokenRefreshView,TokenVerifyView)
from mikazza_api.mikazza.views import CustomObtainTokenPairView

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'addresses',views.AddressViewSet)
router.register(r'properties',views.PropertyViewSet)
router.register(r'rentals',views.RentalViewSet)
router.register(r'tenants',views.TenantViewSet)
router.register(r'contacts',views.ContactViewSet)
router.register(r'documents',views.DocumentViewSet)



schema_view = get_schema_view(
   openapi.Info(
      title="Mikazza API",
      default_version='v1',
      description="Mikazza backend API",
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

# swagger doc: https://appliku.com/post/django-rest-framework-swagger-and-typescript-api-c#swagger-documentation
# and https://pypi.org/project/drf-yasg/#installation

urlpatterns = [
    path('api/v1/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api/v1/auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/v1/docs/',schema_view.with_ui(cache_timeout=0),name='swagger-ui'),
    path('', RedirectView.as_view(url='/api/v1/docs/', permanent=True)),
    path('accounts/', include('allauth.urls')),
    #path("gql/", PrivateGraphQLView.as_view(graphiql=True, schema=schema)),
    path("gql/",GraphQLView.as_view(graphiql=True, schema=schema)),
    #path("gql/",FileUploadGraphQLView.as_view(graphiql=True, schema=schema))
    path('api/v1/jwt/', CustomObtainTokenPairView.as_view(), name='token_obtain_pair'),
    path('api/v1/jwt/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/v1/jwt/verify/', TokenVerifyView.as_view(), name='token_verify'),
]
