from django.contrib.auth.models import Group
from mikazza_api.mikazza.models import *
from rest_framework import viewsets
from rest_framework import permissions
from mikazza_api.mikazza.serializers import *
from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from graphene_django.views import GraphQLView
from rest_framework_simplejwt.views import TokenObtainPairView


def liveliness(request):
    return HttpResponse("OK")

def readiness(request):
    try:
        # Connect to database
        return HttpResponse("OK")
    except Exception as e:
        return HttpResponseServerError("db: cannot connect to database.")

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAdminUser]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAdminUser]

class AddressViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows address to be viewed or edited.
    """
    queryset = Address.objects.all()
    serializer_class = AddressSerializer
    permission_classes = [permissions.IsAuthenticated]
    
    def get_queryset(self):
        return super().get_queryset().filter(createdByUserId=self.request.user)
    
    def perform_create(self, serializer):
            serializer.save(createdByUserId=self.request.user)

class PropertyViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows properties to be viewed or edited.
    """
    queryset = Property.objects.all()
    serializer_class = PropertySerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self): 
        return super().get_queryset().filter(owners__in=[self.request.user])

    def perform_create(self, serializer):
            serializer.save(createdByUserId=self.request.user)
            property_obj = Property.objects.get(pk=serializer.data['id'])
            property_obj.owners.set([self.request.user])
   
class RentalViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows rentales to be viewed or edited.
    """
    queryset = Rental.objects.all()
    serializer_class = RentalSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):                                                   
        return super().get_queryset().filter(createdByUserId=self.request.user)
    
    def perform_create(self, serializer):
            serializer.save(createdByUserId=self.request.user)

class TenantViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows tenants to be viewed or edited.
    """
    queryset = Tenant.objects.all()
    serializer_class = TenantSerializer
    permission_classes = [permissions.IsAuthenticated]
    
    def get_queryset(self):                                            
        return super().get_queryset().filter(createdByUserId=self.request.user)
    
    def perform_create(self, serializer):
            serializer.save(createdByUserId=self.request.user)

class ContactViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows contacts to be viewed or edited.
    """
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
    permission_classes = [permissions.IsAuthenticated]
    
    def get_queryset(self):                                            
        return super().get_queryset().filter(createdByUserId=self.request.user.id)
    
    def perform_create(self, serializer):
            serializer.save(createdByUserId=self.request.user)

class DocumentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows document to be viewed or edited.
    """
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    permission_classes = [permissions.IsAuthenticated]
    
    def perform_create(self, serializer):
        print(self.request.data.get("file").content_type)
        serializer.save(createdByUserId=self.request.user,contentType=self.request.data.get("file").content_type)

class PrivateGraphQLView(LoginRequiredMixin, GraphQLView):
    pass

class CustomObtainTokenPairView(TokenObtainPairView):
    permission_classes = [permissions.AllowAny]
    serializer_class = CustomObtainPairSerializer