from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import AbstractUser, PermissionsMixin, AbstractBaseUser
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from mikazza_api.mikazza.manager import UserManager
from django.conf import settings
from mikazza_api.mikazza.storages import PrivateMediaStorage, PublicMediaStorage
import os


class Address(models.Model):
    name = models.CharField(max_length = 255, blank=False, null=True)
    address = models.CharField(max_length = 255, blank=False, null= False)
    addressExtra = models.CharField(max_length = 255, blank=True, null= True)
    postalCode = models.CharField(max_length = 255, blank=True, null= True)
    city = models.CharField(max_length = 255, blank=True, null= True)
    streetNumber = models.CharField(max_length = 45, blank=True, null= True)
    floorNumber = models.IntegerField(blank=True, null= True)
    doorNumber = models.CharField(max_length = 45, blank=True, null= True)
    country = models.CharField(max_length = 255, blank=True, null= True)
    createdByUserId = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE, blank = False, null = False, auto_created= True, related_name="FK_address_user_id")
    creationDate = models.DateTimeField(auto_now_add = True, auto_now = False, blank = True)

    class Meta:
        verbose_name = _('address')
        verbose_name_plural = _('addresses')

    def __str__(self):
        return self.name

# https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#abstractbaseuser    
class User(AbstractBaseUser, PermissionsMixin):

    def get_upload_path(instance, filename):
        name, ext = os.path.splitext(filename)
        return os.path.join('profiles',str(instance.email), name + ext)

    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)
    is_staff = models.BooleanField(_('staff status'), default=False)
    is_superuser = models.BooleanField(_('admin status'), default=False)
    avatar = models.ImageField(upload_to=get_upload_path, null=True, blank=True, storage=PrivateMediaStorage())

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['is_staff']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        send_mail(subject, message, from_email, [self.email], **kwargs)

class Document(models.Model):

    def get_upload_path(instance, filename):
        name, ext = os.path.splitext(filename)
        return os.path.join('documents', str(instance.fileType),str(instance.createdByUserId.id), slugify(instance.title) + ext)

    TYPES = (
        ('property', 'Property'),
        ('tenant', 'Tenant'),
        ('rental', 'Rental'),
        ('user', 'User'),
    )
    title = models.CharField(max_length=30)
    file = models.FileField(max_length=500, upload_to=get_upload_path, storage=PrivateMediaStorage())
    fileType = models.CharField(max_length=30,choices = TYPES)
    contentType = models.CharField(max_length=30, blank = True, null = True)
    createdByUserId = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE, blank = False, null = False, auto_created= True)
    creationDate = models.DateTimeField(auto_now_add = True, auto_now = False, blank = True)
    
    class Meta:
        verbose_name = _('document')
        verbose_name_plural = _('documents')

    def __str__(self):
        return self.id

class Tenant(models.Model):
    email = models.CharField(max_length = 255, blank=True, null= True)
    name = models.CharField(max_length = 255, blank=True, null= True)
    lastname = models.CharField(max_length = 255, blank=True, null= True)
    picture = models.CharField(max_length = 255, blank=True, null= True)
    addressId = models.ForeignKey(Address, on_delete = models.CASCADE, blank = True, null = True, auto_created= False)
    documents = models.ManyToManyField(Document,blank=True)
    createdByUserId = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE, blank = False, null = False, auto_created= True)
    creationDate = models.DateTimeField(auto_now_add = True, auto_now = False, blank = True)
    
    def __str__(self):
        return self.email
  
    class Meta:
        verbose_name = _('tenant')
        verbose_name_plural = _('tenants')

class Rental(models.Model):
    name = models.CharField(max_length = 255, blank = False, null = False)
    contractStartDate = models.DateTimeField(auto_now_add = False, auto_now = False, blank = True, null= True)
    contractEndDate = models.DateTimeField(auto_now_add = False, auto_now = False, blank = True, null= True)
    rentPrice = models.FloatField(blank=True, null= True)
    rentExpense = models.FloatField(blank=True, null= True)
    waterPayByTenant = models.BooleanField(default = False, blank = True, null= True)
    electricityPayByTenant = models.BooleanField(default = False, blank = True, null= True)
    gasPayByTenant = models.BooleanField(default = False, blank = True, null= True)
    internetPayByTenant = models.BooleanField(default = False, blank = True, null= True)
    rentFurnishedByOwner = models.BooleanField(default = False, blank = True, null= True)
    noticePeriode = models.IntegerField(blank=True, null= True)
    petAccepted = models.BooleanField(default = False, blank = True, null= True)
    smokingAccepted = models.BooleanField(default = False, blank = True, null= True)
    tenants = models.ManyToManyField(Tenant,blank = True)
    documents = models.ManyToManyField(Document,blank = True)
    createdByUserId = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE, blank = False, null = False, auto_created= True)
    creationDate = models.DateTimeField(auto_now_add = True, auto_now = False, blank = True)
    
    class Meta:
        verbose_name = _('rental')
        verbose_name_plural = _('rentals')

    def __str__(self):
        return self.name

class Contact(models.Model):
    type = models.CharField(max_length = 255, blank=True, null= True)
    firstname = models.CharField(max_length = 255, blank=True, null= True)
    lastname = models.CharField(max_length = 255, blank=True, null= True)
    email = models.CharField(max_length = 255, blank=True, null= True)
    telephone = models.CharField(max_length = 255, blank=True, null= True)
    addressId = models.ForeignKey(Address, on_delete = models.CASCADE, blank = True, null = True, auto_created= False)
    documents = models.ManyToManyField(Document,blank=True)
    createdByUserId = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE, blank = False, null = False, auto_created= True)
    creationDate = models.DateTimeField(auto_now_add = True, auto_now = False, blank = True)
    
    class Meta:
        verbose_name = _('contact')
        verbose_name_plural = _('contacts')

    def __str__(self):
        return self.email

class Property(models.Model):
    name =  models.CharField(max_length = 255, blank=False, null= True)
    addressId = models.ForeignKey(Address, on_delete = models.CASCADE, blank = True, null = True, auto_created= False)
    totalSurface = models.FloatField(blank=True, null= True)
    livingSurface =  models.FloatField(blank=True, null= True)
    numberOfRoom = models.IntegerField(blank=True, null= True)
    numberOfBathroom = models.IntegerField(blank=True, null= True)
    numberOfBedroom = models.IntegerField(blank=True, null= True)
    numberOfToilette = models.IntegerField(blank=True, null= True)
    inventoryId = models.IntegerField(blank=True, null= True)
    buyingCost = models.FloatField(blank=True, null= True)
    garageAvailable = models.BooleanField(default = False, blank = True, null= True)
    cellarAvailable = models.BooleanField(default = False, blank = True, null= True)
    numberOfBalcony = models.IntegerField(blank=True, null= True)
    numberOfTerrasse =models.IntegerField(blank=True, null= True)
    gardenAvailable = models.BooleanField(default = False, blank = True, null= True)
    buyingDate = models.DateTimeField(auto_now_add = False, auto_now = False, blank = True, null= True)
    availableDate = models.DateTimeField(auto_now_add = False, auto_now = False, blank = True, null= True)
    sellingDate = models.DateTimeField(auto_now_add = False, auto_now = False, blank = True, null= True)
    source = models.CharField(max_length = 255, blank=True, null= True)
    owners = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="FK_Proerty_userID",blank=True)
    rentals = models.ManyToManyField(Rental,blank=True)
    contacts = models.ManyToManyField(Contact,blank=True)
    documents = models.ManyToManyField(Document,blank=True)
    createdByUserId = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete = models.CASCADE, blank = False, null = False, auto_created= True)
    creationDate = models.DateTimeField(auto_now_add = True, auto_now = False, blank = True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('property')
        verbose_name_plural = _('properties')
