from django.apps import AppConfig


class MikazzaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'mikazza_api.mikazza'
    verbose_name = 'mikazza'
