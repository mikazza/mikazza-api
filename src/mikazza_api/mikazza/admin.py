from django.contrib import admin
from mikazza_api.mikazza.models import *

admin.site.register(User) 
admin.site.register(Address) 
admin.site.register(Property)
admin.site.register(Rental)
admin.site.register(Tenant)
admin.site.register(Contact)
admin.site.register(Document)

