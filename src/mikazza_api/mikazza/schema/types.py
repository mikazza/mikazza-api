import graphene
from graphene import relay
from graphene_django import DjangoObjectType
from django.contrib.auth.models import Group
from mikazza_api.mikazza.models import *

class UserType(DjangoObjectType):
    class Meta:
        model = User
        fields = '__all__'

class GroupType(DjangoObjectType):
    class Meta:
        model = Group
        fields = '__all__'


class AddressType(DjangoObjectType):
    class Meta:
        model = Address
        fields = '__all__'


class PropertyType(DjangoObjectType):
    class Meta:
        model = Property
        fields = '__all__'


class RentalType(DjangoObjectType):
    class Meta:
        model = Rental
        fields = '__all__'


class TenantType(DjangoObjectType):
    class Meta:
        model = Tenant
        fields = '__all__'


class ContactType(DjangoObjectType):
    class Meta:
        model = Contact
        fields = '__all__'

class DocumentType(DjangoObjectType):
    class Meta:
        model = Document
        fields = '__all__'