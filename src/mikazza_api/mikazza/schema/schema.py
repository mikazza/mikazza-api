import graphene
from graphene import ObjectType
from graphene_django import DjangoListField
from mikazza_api.mikazza.schema.types import *
from mikazza_api.mikazza.schema.mutations import *
import graphql_jwt
from graphql_jwt.decorators import login_required, superuser_required

class Query(ObjectType):
    viewer = graphene.Field(UserType, token=graphene.String(required=True))
    users = DjangoListField(UserType)
    user = graphene.Field(UserType, id=graphene.Int())
    groups = DjangoListField(GroupType)
    group = graphene.Field(GroupType, id=graphene.Int())
    addresses = DjangoListField(AddressType)
    address = graphene.Field(AddressType,id=graphene.Int())
    properties = DjangoListField(PropertyType)
    property = graphene.Field(PropertyType,id=graphene.Int())
    rentals = DjangoListField(RentalType)
    rental = graphene.Field(RentalType, id=graphene.Int())
    tenants = DjangoListField(TenantType)
    tenant = graphene.Field(TenantType, id=graphene.Int())
    contacs = DjangoListField(ContactType)
    contact = graphene.Field(ContactType, id=graphene.Int())
    documents = DjangoListField(DocumentType)
    document = graphene.Field(DocumentType, id=graphene.Int())

    @login_required
    def resolve_viewer(self, info, **kwargs):
        return info.context.user

    @superuser_required
    def resolve_users(root, info, **kwargs):
        return User.objects.all()
    
    @superuser_required
    def resolve_user(root, info, id, **kwargs):
        return User.objects.get(pk=id)

    @superuser_required
    def resolve_groups(root, info, **kwargs):
        return Group.objects.all()

    @superuser_required
    def resolve_group(root, info, id, **kwargs):
        return Group.objects.get(pk=id)

    @login_required
    def resolve_addresses(root, info, **kwargs):
        return Address.objects.filter(createdByUserId=info.context.user)

    @login_required
    def resolve_address(root, info, id, **kwargs):
        return Address.objects.get(pk=id,createdByUserId=info.context.user)

    @login_required
    def resolve_properties(root, info, **kwargs):
        return Property.objects.filter(owners__in=[info.context.user])

    @login_required
    def resolve_property(root, info, id, **kwargs):
        return Property.objects.get(pk=id,owners__in=[info.context.user])

    @login_required
    def resolve_rantals(root, info, **kwargs):
        return Rental.objects.filter(createdByUserId=info.context.user)
    
    @login_required
    def resolve_rental(root, info, id, **kwargs):
        return Rental.objects.get(pk=id,createdByUserId=info.context.user)

    @login_required
    def resolve_tenants(root, info, **kwargs):
        return Tenant.objects.get(createdByUserId=info.context.user)
    
    @login_required
    def resolve_tenant(root, info, id, **kwargs):
        return Tenant.objects.get(pk=id, createdByUserId=info.context.user)

    @login_required
    def resolve_contacts(root, info, **kwargs):
        return Contact.objects.get(createdByUserId=info.context.user)
    
    @login_required
    def resolve_contact(root, info, id, **kwargs):
        return Contact.objects.get(pk=id, createdByUserId=info.context.user)

    @login_required
    def resolve_documents(root, info, **kwargs):
        return Document.objects.filter(createdByUserId=info.context.user)
    
    @login_required
    def resolve_document(root, info, id, **kwargs):
        return Document.objects.get(pk=id, createdByUserId=info.context.user)

class Mutation(ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()

    create_user = CreateUserMutation.Field()

    create_address = CreateAddressMutation.Field()
    update_address = UpdateAddressMutation.Field()
    delete_address = DeleteAddressMutation.Field()

    create_tenant = CreateTenantMutation.Field()
    update_tenant = UpdateTenantMutation.Field()
    delete_tenant = DeleteTenantMutation.Field()

    create_rental = CreateRentalMutation.Field()
    update_rental = UpdateRentalMutation.Field()
    delete_rental = DeleteRentalMutation.Field()

    create_property = CreatePropertyMutation.Field()
    update_property = UpdatePropertyMutation.Field()
    delete_property = DeletePropertyMutation.Field()

    create_contact = CreateContactMutation.Field()
    update_contact = UpdateContactMutation.Field()
    delete_contact = DeleteContactMutation.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)