import graphene
from graphene_django.rest_framework.mutation import SerializerMutation
from mikazza_api.mikazza.serializers import *
from mikazza_api.mikazza.models import *
from mikazza_api.mikazza.schema.types import *
from mikazza_api.mikazza.schema.inputs import *
from graphql_jwt.decorators import login_required, superuser_required

# ---------------------------------------------------------------------------- #
#                                     user                                     #
# ---------------------------------------------------------------------------- #

class CreateUserMutation(graphene.Mutation):
    class Arguments:
        object_data = UserInput()
    user = graphene.Field(UserType)

    @classmethod
    @superuser_required
    def mutate(cls, root, info, object_data=None):
        user = User.objects.create(
            email = object_data.email,
            first_name = object_data.first_name,
            last_name = object_data.last_name,
            is_active = object_data.is_active,
            is_staff = object_data.is_staff,
            is_admin = object_data.is_admin

        )

        return CreateUserMutation(user=user)

class UpdateUserMutation(graphene.Mutation):
    class Arguments:
        object_data = UserInput()
    user = graphene.Field(UserType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        user = User.objects.get(pk=info.context.user.id)
        user.email = object_data.email
        user.first_name = object_data.first_name
        user.last_name = object_data.last_name

        return UpdateUserMutation(user=user)

class DeleteUserMutation(graphene.Mutation):
    class Arguments:
         id = graphene.ID()
    
    user = graphene.Field(UserType)

    @classmethod
    @superuser_required
    def mutate(root, info, id):
        user = User.objects.get(pk=info.context.user.id)
        user.delete()
        
        return DeleteUserMutation(user=user)

# ---------------------------------------------------------------------------- #
#                                    Address                                   #
# ---------------------------------------------------------------------------- #
class CreateAddressMutation(graphene.Mutation):
    class Arguments:
        object_data = AddressInput()
    
    address = graphene.Field(AddressType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        address = Address.objects.create(
            name = object_data.name,
            address = object_data.address,
            addressExtra = object_data.addressExtra,
            postalCode = object_data.postalCode,
            city = object_data.city,
            streetNumber = object_data.streetNumber,
            floorNumber = object_data.floorNumber,
            doorNumber = object_data.doorNumber,
            country = object_data.country,
            createdByUserId = info.context.user
        )

        return CreateAddressMutation(address=address)

class UpdateAddressMutation(graphene.Mutation):
    class Arguments:
        object_data = AddressInput()
    
    address = graphene.Field(AddressType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        address = Address.objects.get(pk=id,createdByUserId=info.context.user)
        address.name = object_data.name
        address.address = object_data.address
        address.addressExtra = object_data.addressExtra
        address.postalCode = object_data.postalCode
        address.city = object_data.city
        address.streetNumber = object_data.streetNumber
        address.floorNumber = object_data.floorNumber
        address.doorNumber = object_data.doorNumber
        address.country = object_data.country
        address.createdByUserId = info.context.use
        
        return UpdateAddressMutation(address=address)

class DeleteAddressMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
    
    address = graphene.Field(AddressType)

    @classmethod
    @login_required
    def mutate(root, info, id):
        address = Address.objects.get(pk=id,createdByUserId=info.context.user)

        if address:
            address.delete()

        return DeleteAddressMutation(address=address)

# ---------------------------------------------------------------------------- #
#                                    tenant                                    #
# ---------------------------------------------------------------------------- #
class CreateTenantMutation(graphene.Mutation):
    class Arguments:
        object_data = TenantInput()
    
    tenant = graphene.Field(TenantType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        tenant = Tenant.objects.create(
            email = object_data.email,
            firstname = object_data.name,
            lastname = object_data.lastname,
            createdByUserId = info.context.user
        )

        if object_data.addressId is not None:
            address = Address.objects.get(pk=object_data.addressId)
            if address:
                tenant.addressId = address

        return CreateTenantMutation(tenant=tenant)

class UpdateTenantMutation(graphene.Mutation):
    class Arguments:
        object_data = TenantInput()
    
    tenant = graphene.Field(TenantType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        tenant = Tenant.objects.get(pk=id,createdByUserId=info.context.user)
        tenant.email = object_data.email
        tenant.firstname = object_data.name
        tenant.lastname = object_data.lastname
        
        if object_data.addressId is not None:
            address = Address.objects.get(pk=object_data.addressId)
            if address:
                tenant.addressId = address

        return UpdateTenantMutation(tenant=tenant)

class DeleteTenantMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
    
    tenant = graphene.Field(TenantType)

    @classmethod
    @login_required
    def mutate(root, info, id):
        tenant = Tenant.objects.get(pk=id,createdByUserId=info.context.user)

        if tenant:
            tenant.delete()

        return DeleteTenantMutation(tenant=tenant)

# ---------------------------------------------------------------------------- #
#                                    Rental                                    #
# ---------------------------------------------------------------------------- #
class CreateRentalMutation(graphene.Mutation):
    class Arguments:
        object_data = RentalInput()
    
    rental = graphene.Field(RentalType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        rental = Rental.objects.create(
            name = object_data.name,
            contractStartDate = object_data.contractStartDate,
            contractEndDate = object_data.contractEndDate,
            rentPrice = object_data.rentPrice,
            rentExpense = object_data.rentExpense,
            waterPayByTenant = object_data.waterPayByTenant,
            electricityPayByTenant = object_data.electricityPayByTenant,
            gasPayByTenant = object_data.gasPayByTenant,
            internetPayByTenant = object_data.internetPayByTenant,
            rentFurnishedByOwner = object_data.rentFurnishedByOwner,
            noticePeriode = object_data.noticePeriode,
            petAccepted = object_data.petAccepted,
            smokingAccepted = object_data.smokingAccepted,
            tenants = object_data.tenants,
            documents = object_data.documents,
            createdByUserId = info.context.user
        )

        return CreateRentalMutation(rental=rental)

class UpdateRentalMutation(graphene.Mutation):
    class Arguments:
        object_data = RentalInput()
    
    rental = graphene.Field(RentalType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        rental = Rental.objects.get(pk=id,createdByUserId=info.context.user)
        rental.name = object_data.name
        rental.contractStartDate = object_data.contractStartDate
        rental.contractEndDate = object_data.contractEndDate
        rental.rentPrice = object_data.rentPrice
        rental.rentExpense = object_data.rentExpense
        rental.waterPayByTenant = object_data.waterPayByTenant
        rental.electricityPayByTenant = object_data.electricityPayByTenant
        rental.gasPayByTenant = object_data.gasPayByTenant
        rental.internetPayByTenant = object_data.internetPayByTenant
        rental.rentFurnishedByOwner = object_data.rentFurnishedByOwner
        rental.noticePeriode = object_data.noticePeriode
        rental.petAccepted = object_data.petAccepted
        rental.smokingAccepted = object_data.smokingAccepted
        rental.tenants = object_data.tenants       

        return UpdateRentalMutation(rental=rental)

class DeleteRentalMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    rental = graphene.Field(RentalType)

    @staticmethod
    @login_required
    def mutate(root, info, id):
        rental = Rental.objects.get(pk=id,createdByUserId=info.context.user)
        
        if rental:
            rental.delete()

        return DeleteRentalMutation(contact=contact)

# ---------------------------------------------------------------------------- #
#                                    Contact                                   #
# ---------------------------------------------------------------------------- #
class CreateContactMutation(graphene.Mutation):
    class Arguments:
        object_data = ContactInput()
    
    contact = graphene.Field(ContactType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        contact = Contact.objects.create(
            type = object_data.type,
            name = object_data.name,
            lastname = object_data.lastname,
            telephone = object_data.telephone,
            createdByUserId = info.context.user
        )

        if object_data.addressId is not None:
            address = Address.objects.get(pk=object_data.addressId)
            if address:
                contact.addressId = address

        return CreateContactMutation(contact=contact)

class UpdateContactMutation(graphene.Mutation):
    class Arguments:
        object_data = ContactInput()
    
    contact = graphene.Field(ContactType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        contact = Contact.objects.get(pk=id,createdByUserId=info.context.user)
        if contact:
            contact.type = object_data.type
            contact.name = object_data.name
            contact.lastname = object_data.lastname
            contact.telephone = object_data.telephone
            contact.createdByUserId = info.context.user
        

        if object_data.addressId is not None:
            address = Address.objects.get(pk=object_data.addressId)
            if address:
                contact.addressId = address

        return UpdateContactMutation(contact=contact)

class DeleteContactMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    contact = graphene.Field(ContactType)

    @staticmethod
    @login_required
    def mutate(root, info, id):
        contact = Contact.objects.get(pk=id,createdByUserId=info.context.user)
        
        if contact:
            contact.delete()

        return DeleteContactMutation(contact=contact)

# ---------------------------------------------------------------------------- #
#                                   Property                                   #
# ---------------------------------------------------------------------------- #


class CreatePropertyMutation(graphene.Mutation):
    class Arguments:
        object_data = PropertyInput()
    
    property = graphene.Field(PropertyType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):
        property = Property.objects.create(
            name = object_data.name,
            totalSurface = object_data.totalSurface,
            livingSurface = object_data.livingSurface,
            numberOfRoom = object_data.numberOfRoom,
            numberOfBathroom = object_data.numberOfBathroom,
            numberOfBedroom = object_data.numberOfBedroom,
            numberOfToilette = object_data.numberOfToilette,
            inventoryId = object_data.inventoryId,
            buyingCost = object_data.buyingCost,
            garageAvailable = object_data.garageAvailable,
            cellarAvailable = object_data.cellarAvailable,
            numberOfBalcony = object_data.numberOfBalcony,
            numberOfTerrasse = object_data.numberOfTerrasse,
            gardenAvailable = object_data.gardenAvailable,
            buyingDate = object_data.buyingDate,
            availableDate = object_data.availableDate,
            sellingDate = object_data.sellingDate,
            source = object_data.source,
            createdByUserId = info.context.user
        )

        if object_data.addressId is not None:
            address = Address.objects.get(pk=object_data.addressId)
            if address:
                property.addressId = address
        
        if object_data.rentals is not None:
            for rental_id in object_data.rentals:
                rental_object = Rental.object.get(pk=rental_id)
                if rental_object:
                    property.rentals.add(rental_object)
        
        if object_data.contacts is not None:
            for contact_id in object_data.contacts:
                contact_object = Contact.object.get(pk=contact_id)
                if contact_object:
                    property.contacts.add(contact_object)
        
        if object_data.owners is not None:
            for owner_id in object_data.owners:
                owner_object = Contact.object.get(pk=owner_id)
                if owner_object:
                    property.owners.add(owner_object)
        property.owners.add(info.context.user.id)
        property.save()

        return CreatePropertyMutation(property=property)

class UpdatePropertyMutation(graphene.Mutation):
    class Arguments:
        object_data = PropertyInput()
    
    property = graphene.Field(PropertyType)

    @classmethod
    @login_required
    def mutate(cls, root, info, object_data=None):

        property = Property.objects.get(pk=object_data.id,createdByUserId=info.context.user)
        

        if property:
            property.name = object_data.name
            property.totalSurface = object_data.totalSurface
            property.livingSurface = object_data.livingSurface
            property.numberOfRoom = object_data.numberOfRoom
            property.numberOfBathroom = object_data.numberOfBathroom
            property.numberOfBedroom = object_data.numberOfBedroom
            property.numberOfToilette = object_data.numberOfToilette
            property.inventoryId = object_data.inventoryId
            property.buyingCost = object_data.buyingCost
            property.garageAvailable = object_data.garageAvailable
            property.cellarAvailable = object_data.cellarAvailable
            property.numberOfBalcony = object_data.numberOfBalcony
            property.numberOfTerrasse = object_data.numberOfTerrasse
            property.gardenAvailable = object_data.gardenAvailable
            property.buyingDate = object_data.buyingDate
            property.availableDate = object_data.availableDate
            property.sellingDate = object_data.sellingDate
            property.source = object_data.source
        

        if object_data.addressId is not None:
            address = Address.objects.get(pk=object_data.addressId)
            if address:
                property.addressId = address
        
        if object_data.rentals is not None:
            for rental_id in object_data.rentals:
                rental_object = Rental.object.get(pk=rental_id)
                if rental_object:
                    property.rentals.add(rental_object)
        
        if object_data.contacts is not None:
            for contact_id in object_data.contacts:
                contact_object = Contact.object.get(pk=contact_id)
                if contact_object:
                    property.contacts.add(contact_object)
        
        if object_data.owners is not None:
            for owner_id in object_data.owners:
                owner_object = Contact.object.get(pk=owner_id)
                if owner_object:
                    property.owners.add(owner_object)
        property.save()
        return UpdatePropertyMutation(property=property)

class DeletePropertyMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    property = graphene.Field(PropertyType)

    @staticmethod
    @login_required
    def mutate(root, info, id):
        property = Property.objects.get(pk=id,createdByUserId=info.context.user)
        
        if property:
            property.delete()

        return DeletePropertyMutation(property=property)