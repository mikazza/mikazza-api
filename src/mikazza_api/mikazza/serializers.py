from django.contrib.auth.models import Group
from rest_framework import serializers
from mikazza_api.mikazza.models import *
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'id', 'email','first_name','last_name', 'groups','avatar']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'id','name']


class AddressSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Address
        fields = ['url',
                  "id",
                  "address",
                  "addressExtra",
                  "postalCode",
                  "city",
                  "streetNumber",
                  "floorNumber",
                  "doorNumber",
                  "country", ]


class PropertySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Property
        fields = ["url",
                  "id",
                  "name",
                  "addressId",
                  "totalSurface",
                  "livingSurface",
                  "numberOfRoom",
                  "numberOfBathroom",
                  "numberOfBedroom",
                  "numberOfToilette",
                  "inventoryId",
                  "buyingCost",
                  "garageAvailable",
                  "cellarAvailable",
                  "numberOfBalcony",
                  "numberOfTerrasse",
                  "gardenAvailable",
                  "buyingDate",
                  "availableDate",
                  "sellingDate",
                  "source",
                  "owners",
                  "rentals",
                  "contacts",
                  "documents"]


class RentalSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Rental
        fields = ["url",
                  "id",
                  "name",
                  "contractStartDate",
                  "contractEndDate",
                  "rentPrice",
                  "rentExpense",
                  "waterPayByTenant",
                  "electricityPayByTenant",
                  "gasPayByTenant",
                  "internetPayByTenant",
                  "rentFurnishedByOwner",
                  "noticePeriode",
                  "petAccepted",
                  "smokingAccepted",
                  "tenants",
                  "documents" ]


class TenantSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tenant
        fields = ["url",
                  "id",
                  "email",
                  "name",
                  "lastname",
                  "picture",
                  "addressId",
                  "documents" ]
                  


class ContactSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Contact
        fields = ["url",
                  "id",
                  "type",
                  "firstname",
                  "lastname",
                  "email",
                  "telephone",
                  "addressId", 
                  "documents" ]
                  

class DocumentSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Document
        fields = ["url",
                  "id",
                  "title",
                  "fileType",
                  "file", ]

class CustomObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(CustomObtainPairSerializer, cls).get_token(user)

        # Add custom claims
        token['email'] = user.email
        return token