"""
Django settings for mikazza_api project.

Generated by 'django-admin startproject' using Django 4.0.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/4.0/ref/settings/
"""

from pathlib import Path
import os
from django.core.management import utils
import django_heroku
import django
from django.utils.encoding import force_str
from datetime import timedelta

django.utils.encoding.force_text = force_str


#JWT configuration
SIMPLE_JWT = {
    'USER_ID_FIELD': 'id',
    'USER_ID_CLAIM': 'user_id',
}

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent
print(f"BASE_DIR: {BASE_DIR}")


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("DJANO_SECRET_KEY",utils.get_random_secret_key())

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get('DJANGO_DEBUG',False)

ALLOWED_HOSTS = ['*']
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CSRF_TRUSTED_ORIGINS = [os.environ.get("CSRF_TRUSTED_ORIGINS","http://localhost:8000")]
CORS_ALLOWED_ORIGINS = [os.environ.get("CSRF_TRUSTED_ORIGINS","http://localhost:8000")]
CORS_ORIGIN_WHITELIST = [os.environ.get("CSRF_TRUSTED_ORIGINS","http://localhost:8000")]

# EMAIL_BACKEND = os.environ.get("EMAIL_BACKEND",'django_ses.SESBackend')
# AWS_SES_REGION_NAME = os.environ.get("AWS_SES_REGION_NAME",'eu-west-3')
# AWS_SES_REGION_ENDPOINT = os.environ.get("AWS_SES_REGION_ENDPOINT","feedback-smtp.eu-west-3.amazonses.com")


EMAIL_USE_TLS = True
EMAIL_BACKEND = os.environ.get("EMAIL_BACKEND",'django.core.mail.backends.smtp.EmailBackend')
EMAIL_HOST = os.environ.get("EMAIL_HOST",None)
EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER",None)
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD",None)
EMAIL_PORT = os.environ.get("EMAIL_PORT",587)

# Application definition

INSTALLED_APPS = [
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django_filters',
    'rest_framework',
    'mikazza_api.mikazza.apps.MikazzaConfig',
    'drf_yasg',
    'storages',
    'corsheaders',
    'graphene_django',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.microsoft',
    'graphql_jwt.refresh_token.apps.RefreshTokenConfig',
    'rest_framework_simplejwt',
]

MIDDLEWARE = [
    'mikazza_api.mikazza.middleware.healthcheck.HealthCheckMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',

]

ROOT_URLCONF = 'mikazza_api.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'mikazza_api.wsgi.application'


# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': os.environ.get("DB_DRIVER",'django.db.backends.mysql'),
        'NAME':  os.environ.get("DB_NAME",'mikazza'),
        'USER': os.environ.get("DB_USER",'mikazza'),
        'PASSWORD': os.environ.get("DB_PASSWD",'mikazza'),
        'HOST': os.environ.get("DB_SERVER",'127.0.0.1'),
        'PORT': os.environ.get("DB_PORT","3306"),
        'OPTIONS': {'ssl': os.environ.get("DB_SSL",False)},
    }
}


# Password validation
# https://docs.djangoproject.com/en/4.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
    'graphql_jwt.backends.JSONWebTokenBackend',
]

SITE_ID = 1
AUTH_USER_MODEL = 'mikazza.User'
print(f"CSRF_TRUSTED_ORIGINS: {CSRF_TRUSTED_ORIGINS}")

# ALLAUTH config
# https://django-allauth.readthedocs.io/en/latest/providers.html#okta
ACCOUNT_USER_MODEL_USERNAME_FIELD = None
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_DEFAULT_HTTP_PROTOCOL = os.environ.get("ACCOUNT_DEFAULT_HTTP_PROTOCOL", "http")
#ACCOUNT_EMAIL_VERIFICATION = "optional"
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

SOCIALACCOUNT_PROVIDERS = {
    'microsoft': {
        'tenant': os.environ.get("AZURE_TENANT_ID", None),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/4.0/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_TZ = True

# Default primary key field type
# https://docs.djangoproject.com/en/4.0/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 100,
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    )
}

STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

# S3 storage
# https://www.section.io/engineering-education/how-to-upload-files-to-aws-s3-using-django-rest-framework/
# https://testdriven.io/blog/storing-django-static-and-media-files-on-amazon-s3/
# https://simpleisbetterthancomplex.com/tutorial/2017/08/01/how-to-setup-amazon-s3-in-a-django-project.html

USE_AWS_S3_FOR_STATICFILES = os.getenv('USE_AWS_S3_FOR_STATICFILES', False)

if USE_AWS_S3_FOR_STATICFILES:
    print("S3 storage will be used for statics file")
    AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID")
    AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_ACCESS_KEY_SECRET")
    AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_S3_BUCKET_NAME")
    AWS_S3_CUSTOM_DOMAIN = os.environ.get("AWS_S3_CUSTOM_DOMAIN",f'{AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com')
    AWS_S3_OBJECT_PARAMETERS = {'CacheControl': 'max-age=86400'}
    AWS_S3_SIGNATURE_VERSION = os.environ.get("AWS_S3_SIGNATURE_VERSION",'s3v4')
    AWS_S3_REGION_NAME = os.environ.get("AWS_S3_REGION_NAME",'eu-west-3')
    AWS_S3_FILE_OVERWRITE = os.environ.get("AWS_S3_FILE_OVERWRITE", False)
    AWS_DEFAULT_ACL = os.environ.get("AWS_DEFAULT_ACL", None)
    AWS_S3_VERIFY = os.environ.get("AWS_S3_VERIFY", True)
    AWS_STATIC_LOCATION = os.environ.get("AWS_STATIC_LOCATION", 'static')
    STATIC_URL = f'https://{AWS_S3_CUSTOM_DOMAIN}/{AWS_STATIC_LOCATION }/'
    STATICFILES_STORAGE = 'mikazza_api.mikazza.storages.StaticStorage'
    AWS_PUBLIC_MEDIA_LOCATION = os.environ.get("AWS_PUBLIC_MEDIA_LOCATION"'media/public')
    DEFAULT_FILE_STORAGE = 'mikazza_api.mikazza.storages.PublicMediaStorage'
    AWS_PRIVATE_MEDIA_LOCATION = os.environ.get("AWS_PRIVATE_MEDIA_LOCATION",'media/private')
    PRIVATE_FILE_STORAGE = 'mikazza_api.mikazza.storages.PrivateMediaStorage'

else:
    STATIC_URL =  os.environ.get("STATIC_URL",'staticfiles/')
    STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
    MEDIA_URL = os.environ.get("MEDIA_URL",'mediafiles/')
    MEDIA_ROOT = os.path.join(BASE_DIR, 'mediafiles')
    AWS_STATIC_LOCATION = STATIC_URL
    AWS_PUBLIC_MEDIA_LOCATION = 'media/public'
    AWS_PRIVATE_MEDIA_LOCATION = 'media/private'
    DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'
    STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'


# SWAGGER configuration
SWAGGER_SETTINGS = {
    'USE_SESSION_AUTH': True,
    'LOGOUT_URL': '/accounts/logout',
    'LOGIN_URL': '/accounts/login'
}

# GraphQL
GRAPHENE = {
  'SCHEMA': 'mikazza_api.mikazza.schema.schema',
  "ATOMIC_MUTATIONS": True,
  'MIDDLEWARE': [
        'graphql_jwt.middleware.JSONWebTokenMiddleware',
    ],
}

GRAPHQL_JWT = {
    "JWT_VERIFY_EXPIRATION": True,
    "JWT_LONG_RUNNING_REFRESH_TOKEN": True,
    "JWT_ALLOW_ARGUMENT": True,
}

# activate django heroku
HEROKU_USE_STATICFILES=os.environ.get("HEROKU_USE_STATICFILES",False)
django_heroku.settings(locals(),staticfiles=HEROKU_USE_STATICFILES)