#!/bin/bash
echo "[EXCUTION] create migrations folder"
mkdir mikazza_api/mikazza/migrations
touch mikazza_api/mikazza/migrations/__init__.py

echo "[EXCUTION] makemigrations"
python3 manage.py makemigrations

echo "[EXCUTION] migrate"
python3 manage.py migrate

echo "[EXCUTION] createsuperuser"
python3 manage.py createsuperuser --is_staff True --no-input

echo "[EXCUTION] get static files"
python3 manage.py collectstatic --no-input
