#!/bin/bash
echo "[EXCUTION] start application"
if [ "$PORT" == "" ]; then
  PORT="8000"
fi

gunicorn mikazza_api.wsgi -c gunicorn.conf.py