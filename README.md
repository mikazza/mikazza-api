# Mikazza

## Python virtual env

Create virtual env

```cmd
cd src
poetry shell
```

Activate virtual env
```cmd
cd src
poetry shell
```

Install dependency

```cmd
cd src
poetry install
```

## Docker

Build image

```cmd
docker build -t tlnk/mikazza-api:local .
```

## Docker-compose

Mysql and phpmyadmin container will be deployed. The app will be build.

```cmd
docker-compose up
```

Build

```cmd
docker-compose build
```

## Django manage

Create superuser

```cmd
cd src
poetry run python manage.py createsuperuser --email admin@tlnk.fr --is_staff True
```

### Database management

Prepare migration

```cmd
cd src
poetry run python manage.py makemigrations
```

Migrate database

```cmd
cd src
poetry run python manage.py migrate
```

## ENV var

- DJANO_SECRET_KEY="masupersecretkeyfordjangoapp"
- DB_DRIVER="django.db.backends.mysql"
- DB_USER="mikazza"
- DB_PASSWD="mikazza"
- DB_NAME="mikazza"
- DB_SERVER="localhost"
- DB_PORT="3306"
- DB_SSL="False"
- DJANGO_SUPERUSER_EMAIL="admin@tlnk.fr"
- DJANGO_SUPERUSER_PASSWORD="Bonjour1?"
- USE_AWS_S3_FOR_STATICFILES="TRUE"
- AWS_ACCESS_KEY_ID="aws access key id"
- AWS_ACCESS_KEY_SECRET="aws secret key"
- AWS_S3_BUCKET_NAME="buketname"
- AWS_S3_REGION_NAME="eu-west-3"
- DJANGO_DEBUG="True"
- PORT=8000
- GUNICORN_WORKER=1
- HEROKU_USE_STATICFILES="True"
- AWS_PUBLIC_MEDIA_LOCATION="media/public"
- AWS_PRIVATE_MEDIA_LOCATION="media/private"
- AWS_STATIC_LOCATION="static"
- AWS_S3_FILE_OVERWRITE="False"
- AWS_S3_SIGNATURE_VERSION="s3v4"
- AWS_S3_FILE_OVERWRITE="False"
- AWS_DEFAULT_ACL="None"
- AWS_S3_VERIFY="True"
- AWS_S3_CUSTOM_DOMAIN="buketname.s3.amazonaws.com"
- STATIC_URL="/staticfiles/"
- MEDIA_URL="/mediafiles/"
- CSRF_TRUSTED_ORIGINS="http://localhost:8000"
- AZURE_APP_ID="your application id"
- AZURE_APP_SECRET="your application secret"
- AZURE_TENANT_ID="your azure tenant id"
- ACCOUNT_DEFAULT_HTTP_PROTOCOL="https"
