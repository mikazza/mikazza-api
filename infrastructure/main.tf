/* -------------------------------------------------------------------------- */
/*                     Terraform state with gitlab backend                    */
/* -------------------------------------------------------------------------- */

terraform {
  backend "http" {
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5

  }
}

/* -------------------------------------------------------------------------- */
/*                                     AWS                                    */
/* -------------------------------------------------------------------------- */

data "aws_region" "current" {}
data "azurerm_client_config" "current" {}

locals {
  heroku = {
    region = "eu"
  }
  mikazza = {
    admin_email        = "admin@tlnk.fr"
    admin_username     = "admin"
    name               = "mikazza-api"
    domain             = "mikazza.io"
    azure_zone_rg_name = "TF_PROD_DNS_PUBLIC"
    callback_url       = "accounts/microsoft/login/callback/"

    prod = {
      logo_uri              = "https://i.ibb.co/kqznZZL/logo-green-solo.png"
      heroku_formation_size = "free"
      dns_record_name       = "api"
      ses_domain            = ""
      email                 = "no-reply"


    }

    stage = {
      logo_uri              = "https://i.ibb.co/q7VBpgs/logo-yellow-solo.png"
      heroku_formation_size = "free"
      dns_record_name       = "api.stage"
      ses_domain            = "stage."
      email                 = "no-reply"
    }
  }
}


resource "aws_kms_key" "mikazza_bucket_key_prod" {
  count                   = local.environment == "prod" ? 1 : 0
  description             = "This key is used to encrypt bucket objects for ${local.mikazza.name} application in ${local.environment} environment"
  deletion_window_in_days = 10

  tags = {
    terraform   = "TRUE"
    automatique = "TRUE"
    application = "${local.mikazza.name}"
    environment = "${local.environment}"
  }
}

resource "aws_kms_key" "mikazza_bucket_key_stage" {
  count                   = local.environment == "stage" ? 1 : 0
  description             = "This key is used to encrypt bucket objects for ${local.mikazza.name} application in ${local.environment} environment"
  deletion_window_in_days = 10

  tags = {
    terraform   = "TRUE"
    automatique = "TRUE"
    application = "${local.mikazza.name}"
    environment = "${local.environment}"
  }
}

resource "aws_s3_bucket" "storage_bucket" {
  bucket = "tf-${local.environment}-${local.mikazza.name}"
  acl    = "private"


  versioning {
    enabled = true
  }
  # server_side_encryption_configuration {
  #   rule {
  #     apply_server_side_encryption_by_default {
  #       kms_master_key_id = try(aws_kms_key.mikazza_bucket_key_prod[0].arn, aws_kms_key.mikazza_bucket_key_stage[0].arn)
  #       sse_algorithm     = "aws:kms"
  #     }
  #     bucket_key_enabled = true
  #   }
  # }
  tags = {
    terraform   = "TRUE"
    automatique = "TRUE"
    application = "${local.mikazza.name}"
    environment = "${local.environment}"
  }
}

resource "aws_s3_bucket_public_access_block" "storage_bucket_block_public_access" {
  bucket = aws_s3_bucket.storage_bucket.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false

}

resource "aws_iam_user" "mikazza_svc" {
  name = "tf-svc-${local.environment}-${local.mikazza.name}"

  tags = {
    terraform   = "TRUE"
    automatique = "TRUE"
    application = "${local.mikazza.name}"
    environment = "${local.environment}"
  }
}

resource "aws_iam_access_key" "mikazza_access_key" {
  user = aws_iam_user.mikazza_svc.name
}

resource "aws_iam_group" "mikazza_s3_full_access_group" {
  name = "tf-s3-full-access-${local.environment}-${local.mikazza.name}"
}


resource "aws_iam_group_policy" "pm_policy_s3_full_access" {
  name  = "tf-s3-full-access-${local.environment}-${local.mikazza.name}"
  group = aws_iam_group.mikazza_s3_full_access_group.name

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "VisualEditor0",
        "Effect" : "Allow",
        "Action" : [
          "s3:ListStorageLensConfigurations",
          "s3:ListAccessPointsForObjectLambda",
          "s3:GetAccessPoint",
          "s3:PutAccountPublicAccessBlock",
          "s3:GetAccountPublicAccessBlock",
          "s3:ListAllMyBuckets",
          "s3:ListAccessPoints",
          "s3:PutAccessPointPublicAccessBlock",
          "s3:ListJobs",
          "s3:PutStorageLensConfiguration",
          "s3:ListMultiRegionAccessPoints",
          "s3:CreateJob"
        ],
        "Resource" : "*"
      },
      {
        "Sid" : "VisualEditor1",
        "Effect" : "Allow",
        "Action" : "s3:*",
        "Resource" : ["${aws_s3_bucket.storage_bucket.arn}", "${aws_s3_bucket.storage_bucket.arn}/*"]
      },
      {
        "Sid" : "VisualEditor2",
        "Effect" : "Allow",
        "Action" : [
          "kms:Decrypt",
          "kms:Encrypt",
          "kms:GenerateDataKey",
        ],
        "Resource" : ["${try(aws_kms_key.mikazza_bucket_key_prod[0].arn, aws_kms_key.mikazza_bucket_key_stage[0].arn)}"]
      }
    ]
  })
}

resource "aws_iam_group_membership" "pm_app_group_membership" {
  name = "tf-group-membership-${local.environment}-${local.mikazza.name}"

  users = [
    aws_iam_user.mikazza_svc.name,
  ]

  group = aws_iam_group.mikazza_s3_full_access_group.name
}

resource "random_password" "django_secret" {
  length  = 48
  special = false
}

resource "random_password" "django_admin_pwd" {
  length  = 20
  special = false
}

/* -------------------------------------------------------------------------- */
/*                                   Heroku                                   */
/* -------------------------------------------------------------------------- */

resource "heroku_app" "mikazza_app_api" {
  name   = "${local.environment}-${local.mikazza.name}"
  region = local.heroku.region

}

resource "heroku_app_config_association" "mikazza_app_api" {
  app_id = heroku_app.mikazza_app_api.id
  sensitive_vars = {
    DJANO_SECRET_KEY              = random_password.django_secret.result
    DJANGO_SUPERUSER_EMAIL        = local.mikazza.admin_email
    DJANGO_SUPERUSER_PASSWORD     = random_password.django_admin_pwd.result
    AWS_S3_BUCKET_ARN             = aws_s3_bucket.storage_bucket.arn
    AWS_S3_BUCKET_NAME            = aws_s3_bucket.storage_bucket.bucket
    AWS_ACCESS_KEY_ID             = aws_iam_access_key.mikazza_access_key.id
    AWS_ACCESS_KEY_SECRET         = aws_iam_access_key.mikazza_access_key.secret
    AWS_S3_REGION_NAME            = data.aws_region.current.name
    USE_AWS_S3_FOR_STATICFILES    = "true"
    ENV                           = local.environment
    DB_NAME                       = scaleway_rdb_database.database.name
    DB_PASSWD                     = random_password.user_password.result
    DB_PORT                       = data.scaleway_rdb_instance.instance.endpoint_port
    DB_SERVER                     = data.scaleway_rdb_instance.instance.endpoint_ip
    DB_USER                       = scaleway_rdb_user.database_user.name
    AZURE_TENANT_ID               = data.azurerm_client_config.current.tenant_id
    CSRF_TRUSTED_ORIGINS          = trimsuffix(heroku_app.mikazza_app_api.web_url, "/")
    ACCOUNT_DEFAULT_HTTP_PROTOCOL = "https"
    AZURE_APP_ID                  = azuread_application.azure_app.application_id
    AZURE_APP_SECRET              = azuread_application_password.azure_app_secret.value

  }
}

resource "heroku_formation" "mikazza_formation" {
  count    = local.mikazza[local.environment].heroku_formation_size == "free" ? 0 : 1
  app      = heroku_app.mikazza_app_api.name
  type     = "web"
  size     = local.mikazza[local.environment].heroku_formation_size
  quantity = 1
}


resource "heroku_domain" "mikazza_custom_domain" {
  app      = heroku_app.mikazza_app_api.name
  hostname = "${local.mikazza[local.environment].dns_record_name}.${local.mikazza.domain}"
}

/* -------------------------------------------------------------------------- */
/*                                     DNS                                    */
/* -------------------------------------------------------------------------- */

resource "azurerm_dns_cname_record" "mikazza_app_record" {
  name                = local.mikazza[local.environment].dns_record_name
  zone_name           = local.mikazza.domain
  resource_group_name = local.mikazza.azure_zone_rg_name
  ttl                 = 300
  record              = heroku_domain.mikazza_custom_domain.cname
}


/* -------------------------------------------------------------------------- */
/*                                  Database                                  */
/* -------------------------------------------------------------------------- */

data "scaleway_rdb_instance" "instance" {
  name = "prod-database01"
}

resource "scaleway_rdb_database" "database" {
  instance_id = data.scaleway_rdb_instance.instance.id
  name        = "tf-${local.environment}-${local.mikazza.name}"
}

resource "random_password" "user_password" {
  length  = 32
  special = true
}

resource "scaleway_rdb_user" "database_user" {
  instance_id = data.scaleway_rdb_instance.instance.id
  name        = "tf-${local.environment}-${local.mikazza.name}"
  password    = random_password.user_password.result
  is_admin    = false
}

resource "scaleway_rdb_privilege" "database_privilege" {
  instance_id   = data.scaleway_rdb_instance.instance.id
  user_name     = scaleway_rdb_user.database_user.name
  database_name = scaleway_rdb_database.database.name
  permission    = "all"
}

/* -------------------------------------------------------------------------- */
/*                                    Azure                                   */
/* -------------------------------------------------------------------------- */

resource "azuread_application" "azure_app" {
  display_name     = "${local.environment}-${local.mikazza.name}"
  sign_in_audience = "AzureADMyOrg"

  web {
    homepage_url  = heroku_app.mikazza_app_api.web_url
    redirect_uris = ["https://${heroku_domain.mikazza_custom_domain.hostname}/${local.mikazza.callback_url}", "${heroku_app.mikazza_app_api.web_url}${local.mikazza.callback_url}"]

    implicit_grant {
      access_token_issuance_enabled = true
      id_token_issuance_enabled     = true
    }
  }
}

resource "azuread_application_password" "azure_app_secret" {
  application_object_id = azuread_application.azure_app.object_id
}

/* -------------------------------------------------------------------------- */
/*                                    Email                                   */
/* -------------------------------------------------------------------------- */

# resource "aws_ses_domain_identity" "app" {
#   domain = "${local.mikazza[local.environment].ses_domain}${local.mikazza.domain}"
# }

# resource "aws_ses_domain_mail_from" "app" {
#   domain           = aws_ses_domain_identity.app.domain
#   mail_from_domain = "${local.mikazza[local.environment].ses_domain}${local.mikazza.domain}"
# }

# resource "aws_ses_email_identity" "app" {
#   email = "${local.mikazza[local.environment].email}@${aws_ses_domain_identity.app.domain}"
# }

# resource "azurerm_dns_mx_record" "ses" {
#   name                = aws_ses_domain_mail_from.app.mail_from_domain
#   zone_name           = local.mikazza.domain
#   resource_group_name = local.mikazza.azure_zone_rg_name
#   ttl                 = 300

#   record {
#     preference = 10
#     exchange   = local.mikazza.smtp_server
#   }

#   tags = {
#     terraform   = "TRUE"
#     automatique = "TRUE"
#     application = "${local.mikazza.name}"
#     environment = "${local.environment}"
#   }
# }

# resource "azurerm_dns_txt_record" "ses" {
#   name                = aws_ses_domain_mail_from.app.mail_from_domain
#   zone_name           = local.mikazza.domain
#   resource_group_name = local.mikazza.azure_zone_rg_name
#   ttl                 = 300

#   record {
#     value = "v=spf1 include:amazonses.com -all"
#   }

#   tags = {
#     terraform   = "TRUE"
#     automatique = "TRUE"
#     application = "${local.mikazza.name}"
#     environment = "${local.environment}"
#   }
# }

# resource "azurerm_dns_txt_record" "ses_verification" {
#   name                = "_amazonses.${aws_ses_domain_identity.app.id}"
#   zone_name           = local.mikazza.domain
#   resource_group_name = local.mikazza.azure_zone_rg_name
#   ttl                 = 300

#   record {
#     value = aws_ses_domain_identity.app.verification_token
#   }

#   tags = {
#     terraform   = "TRUE"
#     automatique = "TRUE"
#     application = "${local.mikazza.name}"
#     environment = "${local.environment}"
#   }
# }

# resource "aws_ses_domain_identity_verification" "ses_verification" {
#   domain = aws_ses_domain_identity.app.id

#   depends_on = [azurerm_dns_txt_record.ses_verification]
# }

# data "aws_iam_policy_document" "ses_sender" {
#   statement {
#     actions   = ["ses:SendRawEmail"]
#     resources = ["*"]
#   }
# }

# resource "aws_iam_policy" "ses_sender" {
#   name        = "ses_sender"
#   description = "Allows sending of e-mails via Simple Email Service"
#   policy      = data.aws_iam_policy_document.ses_sender.json
# }

# resource "aws_iam_user_policy_attachment" "ses_attachment" {
#   user       = aws_iam_user.mikazza_svc.name
#   policy_arn = aws_iam_policy.ses_sender.arn
# }

# resource "aws_ses_domain_dkim" "ses" {
#   domain = aws_ses_domain_identity.app.domain
# }

# resource "azurerm_dns_cname_record" "ses_dkim" {
#   count               = 3
#   name                = "${element(aws_ses_domain_dkim.ses.dkim_tokens, count.index)}._domainkey"
#   zone_name           = local.mikazza.domain
#   resource_group_name = local.mikazza.azure_zone_rg_name
#   ttl                 = 300
#   record              = "${element(aws_ses_domain_dkim.ses.dkim_tokens, count.index)}.dkim.amazonses.com"
# }